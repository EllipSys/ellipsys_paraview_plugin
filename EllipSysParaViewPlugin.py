from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smdomain,
    smhint,
    smproperty,
    smproxy,
)
import numpy as np
from vtkmodules.vtkCommonCore import vtkDataArraySelection
import os
from vtk.numpy_interface import dataset_adapter as dsa
from vtk import (vtkMultiBlockDataSet, vtkPoints, vtkStructuredGrid, vtkMatrix4x4,
                 vtkTransformFilter, vtkTransform, vtkArrayCalculator, vtkCompositeDataSet)
# -------- XXD reader --------- #

@smproxy.reader(
    name="EllipSys XXD file reader",
    extensions=["X3D", "X2D"],
    file_description="EllipSys XXD file",
    support_reload=True,
)
class EllipSysXXDReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet"
        )
        self._filename = None
        self._load_attr = True
        self._blocks = None
        self._load_ghost = False
        self._is2D = None

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=["X3D", "X2D"],
        file_description="EllipSys XXD file"
    )
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
            name="Load attr"
            label="Load attr (boundary conditions)"
            command="SetLoadAttr"
            default_values="1"
            number_of_elements="1">
            <BooleanDomain name="bool" />
        </IntVectorProperty>""")
    def SetLoadAttr(self, load_attr):
        if self._load_attr != load_attr:
            self._load_attr = load_attr
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
                name="Load ghost cells"
                label="Load ghost cells"
                command="SetLoadGhost"
                default_values="0"
                number_of_elements="1">
                <BooleanDomain name="bool" />
            </IntVectorProperty>""")
    def SetLoadGhost(self, load_ghost):
        if self._load_ghost != load_ghost:
            self._load_ghost = load_ghost
            self.Modified()

    @smproperty.stringvector(name="Blocks", default_values="All")
    def SetBlocks(self, blocks_str):
        if blocks_str is None:
            self._blocks = None
            self.Modified()
        elif "None" in blocks_str:
            self._blocks = None
            self.Modified()
        elif "All" in blocks_str:
            self._blocks = None
            self.Modified()
        else:
            blocks = []
            for val in blocks_str.split(","):
                if ":" in val:
                    vals = [int(va) for va in val.split(":")]
                    if not vals:
                        break
                    blocks += list(range(*vals))
                else:
                    blocks += [int(val)]
            self._blocks = blocks
            self.Modified()

    def RequestData(self, request, inInfoVec, outInfoVec):
        # Initalizing the vtkMultiBlockDataSet
        output = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(outInfoVec))


        # Reading X3D file and setting data as m-blocks
        XXD2vtk(self._filename, self._load_attr, self._blocks, self._load_ghost, self._is2D, output)
        return 1

# -------- RST reader --------- #

@smproxy.reader(
    name="EllipSys RST file reader",
    extensions=["RST.01"],
    file_description="EllipSys RST file",
    support_reload=False,
)
class EllipSysRSTReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet"
        )
        # Initilize filename vars
        self._filename = None
        self._project_name = None
        self._RSTpath = None
        self._filename_grid = None
        self._blocks = None
        self._load_ghost = False
        self._is2D = None

        # Selection arrays
        self._field_names = vtkDataArraySelection()
        # Adding observers (adding Modification event)
        self._field_names.AddObserver("ModifiedEvent", createModifiedCallback(self))

    @smproperty.stringvector(name="FileName", label="RST File")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=["RST.01"],
        file_description="EllipSys RST file"
    )
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self._RSTpath = os.path.dirname(filename)
            self._project_name = os.path.basename(filename).replace(".RST.01", "")
            if self._filename_grid is None:
                self.SetXXDFilename_from_projname()
            self.Modified()

    def SetXXDFilename_from_projname(self):
        if os.path.isfile(os.path.join(self._RSTpath, self._project_name + ".X3D")):
            self._filename_grid = os.path.join(self._RSTpath, self._project_name + ".X3D")
        elif os.path.isfile(os.path.join(self._RSTpath, self._project_name + ".X2D")):
            self._filename_grid = os.path.join(self._RSTpath, self._project_name + ".X2D")
        self.Modified()

    @smproperty.stringvector(name="XXDfilename", default_values="From project name", label="XXD File")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=["X3D", "X2D"],
        file_description="EllipSys X3D or X2D file"
    )
    def SetXXDFileName(self, filename):
        if not filename:
            self.SetXXDFilename_from_projname()
        elif not (("X3D" in filename) or ("X2D" in filename)):
            self.SetXXDFilename_from_projname()
        else:
            self._filename_grid = filename
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
                    name="Load ghost cells"
                    label="Load ghost cells"
                    command="SetLoadGhost"
                    default_values="0"
                    number_of_elements="1">
                    <BooleanDomain name="bool" />
                </IntVectorProperty>""")
    def SetLoadGhost(self, load_ghost):
        if self._load_ghost != load_ghost:
            self._load_ghost = load_ghost
            self.Modified()

    @smproperty.stringvector(name="Blocks", default_values="All")
    def SetBlocks(self, blocks_str):
        if blocks_str is None:
            self._blocks = None
            self.Modified()
        elif "None" in blocks_str:
            self._blocks = None
            self.Modified()
        elif "All" in blocks_str:
            self._blocks = None
            self.Modified()
        else:
            blocks = []
            for val in blocks_str.split(","):
                if ":" in val:
                    vals = [int(va) for va in val.split(":")]
                    if not vals:
                        break
                    blocks += list(range(*vals))
                else:
                    blocks += [int(val)]
            self._blocks = blocks
            self.Modified()

    @smproperty.dataarrayselection(name="Field Data")
    def GetScalarDataSelection(self):
        return self._field_names

    def RequestInformation(self, request, inInfoVec, outInfoVec):
        executive = self.GetExecutive()
        outInfo = outInfoVec.GetInformationObject(0)

        field_names = sim2names(self._filename)

        # Setting scalar cell names to selection array
        for name in field_names:
            self._field_names.AddArray(name, name == "p")
        self._field_names.AddArray("Vel", True)

        return 1

    def RequestData(self, request, inInfoVec, outInfoVec):
        # Initilizing multiblock
        output = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(outInfoVec))

        load_names = set()
        for i in range(self._field_names.GetNumberOfArrays()):
            sname = self._field_names.GetArrayName(i)
            if self._field_names.ArrayIsEnabled(sname):
                load_names.add(sname)

        mblock = sim2vtk(self._filename, self._filename_grid, load_names, False, self._blocks,
                         self._load_ghost, self._is2D, None, output)

        return 1


# From: https://github.com/Kitware/ParaView/blob/master/Examples/Plugins/PythonAlgorithm/PythonAlgorithmExamples.py
def createModifiedCallback(anobject):
    import weakref
    weakref_obj = weakref.ref(anobject)
    anobject = None

    def _markmodified(*args, **kwars):
        o = weakref_obj()
        if o is not None:
            o.Modified()

    return _markmodified



# -------- adinput reader --------- #

@smproxy.reader(
    name="EllipSys adinput.dat file reader",
    filename_patterns="adinput.dat",
    file_description="EllipSys adinput.dat file",
    support_reload=False,
)
class EllipSysADinputReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet"
        )
        self._filename = None
        self._ADs = None
        self._close_disc = True
        self._projname = "grid"

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self.Modified()

    @smproperty.stringvector(name="ProjectName", default_values="grid")
    def ProjectName(self, projname):
        if self._projname != projname:
            self._projname = projname
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
        name="SetCloseDisc"
        label="Close Disc Mesh"
        command="SetCloseDisc"
        default_values="1"
        number_of_elements="1">
        <BooleanDomain name="bool" />
    </IntVectorProperty>""")
    def SetCloseDisc(self, close_disc):
        if self._close_disc != close_disc:
            self._close_disc = close_disc
            self.Modified()

    @smproperty.stringvector(name="ADs", default_values="All")
    def SetADs(self, ADs_str):
        if ADs_str is None:
            self._ADs = None
            self.Modified()
        elif "None" in ADs_str:
            self._ADs = None
            self.Modified()
        elif "All" in ADs_str:
            self._ADs = None
            self.Modified()
        else:
            blocks = []
            for val in ADs_str.split(","):
                if ":" in val:
                    vals = [int(va) for va in val.split(":")]
                    if not vals:
                        break
                    blocks += list(range(*vals))
                else:
                    blocks += [int(val)]
            self._ADs = blocks
            self.Modified()

    def RequestData(self, request, inInfoVec, outInfoVec):
        # Initalizing the vtkMultiBlockDataSet
        output = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(outInfoVec))

        output = adinput2vtk(filename=self._filename, ADs=self._ADs, close_disc=self._close_disc, mblock=output)
        return 1


class FortranFile_wskip(object):
    """
    A file object for unformatted sequential files from Fortran code.
    Modified version of the scipy implementation where skip_record method is added.
    Parameters
    ----------
    filename : file or str
        Open file object or filename.
    mode : {'r', 'w'}, optional
        Read-write mode, default is 'r'.
    header_dtype : dtype, optional
        Data type of the header. Size and endiness must match the input/output file.
    Notes
    -----
    These files are broken up into records of unspecified types. The size of
    each record is given at the start (although the size of this header is not
    standard) and the data is written onto disk without any formatting. Fortran
    compilers supporting the BACKSPACE statement will write a second copy of
    the size to facilitate backwards seeking.
    This class only supports files written with both sizes for the record.
    It also does not support the subrecords used in Intel and gfortran compilers
    for records which are greater than 2GB with a 4-byte header.
    An example of an unformatted sequential file in Fortran would be written as::
        OPEN(1, FILE=myfilename, FORM='unformatted')
        WRITE(1) myvariable
    Since this is a non-standard file format, whose contents depend on the
    compiler and the endianness of the machine, caution is advised. Files from
    gfortran 4.8.0 and gfortran 4.1.2 on x86_64 are known to work.
    Consider using Fortran direct-access files or files from the newer Stream
    I/O, which can be easily read by `numpy.fromfile`.
    Examples
    --------
    To create an unformatted sequential Fortran file:
    >>> from scipy.io import FortranFile
    >>> f = FortranFile('test.unf', 'w')
    >>> f.write_record(np.array([1,2,3,4,5], dtype=np.int32))
    >>> f.write_record(np.linspace(0,1,20).reshape((5,4)).T)
    >>> f.close()
    To read this file:
    >>> f = FortranFile('test.unf', 'r')
    >>> print(f.read_ints(np.int32))
    [1 2 3 4 5]
    >>> print(f.read_reals(float).reshape((5,4), order="F"))
    [[0.         0.05263158 0.10526316 0.15789474]
     [0.21052632 0.26315789 0.31578947 0.36842105]
     [0.42105263 0.47368421 0.52631579 0.57894737]
     [0.63157895 0.68421053 0.73684211 0.78947368]
     [0.84210526 0.89473684 0.94736842 1.        ]]
    >>> f.close()
    Or, in Fortran::
        integer :: a(5), i
        double precision :: b(5,4)
        open(1, file='test.unf', form='unformatted')
        read(1) a
        read(1) b
        close(1)
        write(*,*) a
        do i = 1, 5
            write(*,*) b(i,:)
        end do
    """

    def __init__(self, filename, mode='r', header_dtype=np.uint32):
        if header_dtype is None:
            raise ValueError('Must specify dtype')

        header_dtype = np.dtype(header_dtype)
        if header_dtype.kind != 'u':
            warnings.warn("Given a dtype which is not unsigned.")

        if mode not in 'rw' or len(mode) != 1:
            raise ValueError('mode must be either r or w')

        if hasattr(filename, 'seek'):
            self._fp = filename
        else:
            self._fp = open(filename, '%sb' % mode)

        self._header_dtype = header_dtype

    def _read_size(self, eof_ok=False):
        n = self._header_dtype.itemsize
        b = self._fp.read(n)
        if (not b) and eof_ok:
            raise FortranEOFError("End of file occurred at end of record")
        elif len(b) < n:
            raise FortranFormattingError(
                "End of file in the middle of the record size")
        return int(np.frombuffer(b, dtype=self._header_dtype, count=1))

    def write_record(self, *items):
        """
        Write a record (including sizes) to the file.
        Parameters
        ----------
        *items : array_like
            The data arrays to write.
        Notes
        -----
        Writes data items to a file::
            write_record(a.T, b.T, c.T, ...)
            write(1) a, b, c, ...
        Note that data in multidimensional arrays is written in
        row-major order --- to make them read correctly by Fortran
        programs, you need to transpose the arrays yourself when
        writing them.
        """
        items = tuple(np.asarray(item) for item in items)
        total_size = sum(item.nbytes for item in items)

        nb = np.array([total_size], dtype=self._header_dtype)

        nb.tofile(self._fp)
        for item in items:
            item.tofile(self._fp)
        nb.tofile(self._fp)

    def read_record(self, *dtypes, **kwargs):
        """
        Reads a record of a given type from the file.
        Parameters
        ----------
        *dtypes : dtypes, optional
            Data type(s) specifying the size and endiness of the data.
        Returns
        -------
        data : ndarray
            A one-dimensional array object.
        Raises
        ------
        FortranEOFError
            To signal that no further records are available
        FortranFormattingError
            To signal that the end of the file was encountered
            part-way through a record
        Notes
        -----
        If the record contains a multi-dimensional array, you can specify
        the size in the dtype. For example::
            INTEGER var(5,4)
        can be read with::
            read_record('(4,5)i4').T
        Note that this function does **not** assume the file data is in Fortran
        column major order, so you need to (i) swap the order of dimensions
        when reading and (ii) transpose the resulting array.
        Alternatively, you can read the data as a 1D array and handle the
        ordering yourself. For example::
            read_record('i4').reshape(5, 4, order='F')
        For records that contain several variables or mixed types (as opposed
        to single scalar or array types), give them as separate arguments::
            double precision :: a
            integer :: b
            write(1) a, b
            record = f.read_record('<f4', '<i4')
            a = record[0]  # first number
            b = record[1]  # second number
        and if any of the variables are arrays, the shape can be specified as
        the third item in the relevant dtype::
            double precision :: a
            integer :: b(3,4)
            write(1) a, b
            record = f.read_record('<f4', np.dtype(('<i4', (4, 3))))
            a = record[0]
            b = record[1].T
        Numpy also supports a short syntax for this kind of type::
            record = f.read_record('<f4', '(3,3)<i4')
        See Also
        --------
        read_reals
        read_ints
        """
        dtype = kwargs.pop('dtype', None)
        skip_read = kwargs.pop("skip", False)  # Added comapared to Scipy version
        if kwargs:
            raise ValueError("Unknown keyword arguments {}".format(tuple(kwargs.keys())))

        if dtype is not None:
            dtypes = dtypes + (dtype,)
        elif not dtypes:
            raise ValueError('Must specify at least one dtype')

        first_size = self._read_size(eof_ok=True)

        dtypes = tuple(np.dtype(dtype) for dtype in dtypes)
        block_size = sum(dtype.itemsize for dtype in dtypes)

        num_blocks, remainder = divmod(first_size, block_size)
        if remainder != 0:
            raise ValueError('Size obtained ({0}) is not a multiple of the '
                             'dtypes given ({1}).'.format(first_size, block_size))

        if len(dtypes) != 1 and first_size != block_size:
            # Fortran does not write mixed type array items in interleaved order,
            # and it's not possible to guess the sizes of the arrays that were written.
            # The user must specify the exact sizes of each of the arrays.
            raise ValueError('Size obtained ({0}) does not match with the expected '
                             'size ({1}) of multi-item record'.format(first_size, block_size))

        data = []
        for dtype in dtypes:
            if skip_read:  # Added comapared to Scipy version
                self._fp.seek(block_size * num_blocks, 1)
            else:
                r = np.fromfile(self._fp, dtype=dtype, count=num_blocks)
                if len(r) != num_blocks:
                    raise FortranFormattingError(
                        "End of file in the middle of a record")
                if dtype.shape != ():
                    # Squeeze outmost block dimension for array items
                    if num_blocks == 1:
                        assert r.shape == (1,) + dtype.shape
                        r = r[0]

                data.append(r)

        second_size = self._read_size()
        if first_size != second_size:
            raise IOError('Sizes do not agree in the header and footer for '
                          'this record - check header dtype')

        # Unpack result
        if skip_read:  # Added comapared to Scipy version
            return None
        elif len(dtypes) == 1:
            return data[0]
        else:
            return tuple(data)

    def skip_record(self, *dtypes, **kwargs):  # Added comapared to Scipy version
        return self.read_record(*dtypes, **kwargs, skip=True)

    def read_block(self, dtype, load_ghost=False, bsize=None, is2D=False):
        if load_ghost:
            return self.read_record(dtype)
        else:
            if is2D:
                sli = (slice(1, -1), slice(1, -1))
                reshape = [bsize, bsize]
            else:
                sli = (slice(1, -1), slice(1, -1), slice(1, -1))
                reshape = [bsize, bsize, bsize]
            return self.read_record(dtype).reshape(reshape)[sli].flatten()

    def read_ints(self, dtype='i4'):
        """
        Reads a record of a given type from the file, defaulting to an integer
        type (``INTEGER*4`` in Fortran).
        Parameters
        ----------
        dtype : dtype, optional
            Data type specifying the size and endiness of the data.
        Returns
        -------
        data : ndarray
            A one-dimensional array object.
        See Also
        --------
        read_reals
        read_record
        """
        return self.read_record(dtype)

    def read_reals(self, dtype='f8'):
        """
        Reads a record of a given type from the file, defaulting to a floating
        point number (``real*8`` in Fortran).
        Parameters
        ----------
        dtype : dtype, optional
            Data type specifying the size and endiness of the data.
        Returns
        -------
        data : ndarray
            A one-dimensional array object.
        See Also
        --------
        read_ints
        read_record
        """
        return self.read_record(dtype)

    def close(self):
        """
        Closes the file. It is unsupported to call any other methods off this
        object after closing it. Note that this class supports the 'with'
        statement in modern versions of Python, to call this automatically
        """
        self._fp.close()

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.close()


def RST2dict(filename, load_names=None, read_default=True, blocks=None, load_ghost=False, as_matrix=False,
             is2D=False, vectors=None):
    # Setting default arguments
    load_names, vectors, fields_in_vectors = RST_set_default_args(load_names, read_default, vectors)

    bsize = None
    nblock = None
    data_out = dict()
    with FortranFile_wskip(filename, "r") as file:
        # Reading header
        read_restart_version(file)

        # Reading main file data
        read_more = True
        while (read_more):
            # Reading data header (name, data-type, array_type, size)
            name, dtype, action, size = read_data_header(file)

            # Reading data
            if name == "bsize":
                bsize = read_data(file, dtype, action, size, nblock, True)
                # Inialize arrays
                ppbs_wghost = bsize + 3  # +3 -> 2 for ghost 1 for vertex and not cell
                if load_ghost:  # nppb -> points per block side
                    ppbs = ppbs_wghost
                else:
                    ppbs = bsize + 1
            elif name == "nblock":
                nblock = read_data(file, dtype, action, size, nblock, True)
                if blocks is None:
                    blocks = range(1, nblock + 1)
                nblock_out = len(blocks)
            elif name == "stop":
                read_more = False
            elif (name in load_names) or ((name in fields_in_vectors) and (fields_in_vectors[name] in load_names)):

                dtype_np = float if dtype == "REAL" else int

                if name in fields_in_vectors:  # Reading vector field
                    vec_name = fields_in_vectors[name]
                    if vec_name not in data_out:
                        data_out[vec_name] = initialize_data_array(nblock_out, ppbs, dtype_np, True, is2D)
                    data_out[vec_name][:,
                    vectors[vec_name].index(name), :] = read_data(file, dtype, action, size, nblock,
                                                                  data_out[vec_name][:, vectors[vec_name].index(name),
                                                                  :],
                                                                  blocks, load_ghost, ppbs, is2D)
                else:  # Reading scalar field
                    data_out[name] = read_data(file, dtype, action, size, nblock, None, blocks, load_ghost, ppbs, is2D)


            else:
                read_data(file, dtype, action, size, nblock, skip=True)

    if as_matrix:
        for key, data in data_out.items():
            mat_size = (ppbs, ppbs) if is2D else (ppbs, ppbs, ppbs)

            if len(data.shape) == 3:
                data_out[key] = data.reshape((nblock_out, 3) + mat_size)
            else:
                data_out[key] = data.reshape((nblock_out,) + mat_size)
    return data_out, bsize, nblock


def RST2names(filename, name_out="fields"):
    """ Reads names in .RST file
        name_out='fields' -> only data fields (same shape as grid data)
        name_out='fields info' -> only data fields as well as information about the data (dtype, action, size)
        name_out='all' -> outputs all data names
        name_out='all info' -> returns all data names and information about the data (dtype, action, size)
    """
    if "info" in name_out:
        data_out = dict()
    else:
        data_out = set()

    with FortranFile_wskip(filename, "r") as file:
        # Reading header
        read_restart_version(file)

        # Creating ouput data lists
        read_more = True
        nblock = None
        while (read_more):
            # Reading data header (name, data-type, array_type, size)
            name, dtype, action, size = read_data_header(file)

            # Reading data (only reading nblock)
            data = read_data(file, dtype, action, size, nblock, skip=not name == "nblock")

            # Setting bsize, nblock variables and testing for stop read
            if name == "stop":
                read_more = False
            elif name == "nblock":
                nblock = data

            if "fields" in name_out:
                if is_data_field(dtype, action):
                    if "info" in name_out:
                        data_out[name] = dict(dtype=dtype, action=action, size=size)
                    else:
                        data_out.add(name)
            elif "all" in name_out:
                if "info" in name_out:
                    data_out[name] = dict(dtype=dtype, action=action, size=size)
                else:
                    data_out.add(name)
        return data_out


def RST_set_default_args(load_names, read_default, vectors):
    # Updating default settings
    if load_names is None:
        load_names = set()
    if read_default:
        load_names.update(["p", "Vel"])
    if vectors is None:
        vectors = dict()

    # Default vector for velocity
    if not any("u" in fields for fields in vectors.values()):
        vectors["Vel"] = ["u", "v", "w"]

    # Dict containing field -> vector connection
    fields_in_vectors = dict()
    for name, fields in vectors.items():
        for field in fields:
            fields_in_vectors[field] = name
    return load_names, vectors, fields_in_vectors


def XXD2dict(filename, load_names=None, read_default=True, blocks=None, load_ghost=False, as_matrix=False, is2D=None):
    """
    Reading EllipSys .X3D or .X2D files and return dict with data and bsize and nblock
    """
    load_names, is2D = XXD_set_default_args(filename, load_names, read_default, is2D)

    # Reading X3D file
    data_out = dict()
    with FortranFile_wskip(filename, "r") as file:
        # Reading header
        bsize = int(file.read_ints()[0])
        nblock = int(file.read_ints()[0])
        if is2D:  # Not sure what this line is for in the 2D grid?! - Kenneth
            _ = file.read_ints()

        if blocks is None:
            blocks = range(1, nblock + 1)

        nblock_out = len(blocks)

        # Inialize arrays
        ppbs_wghost = bsize + 3  # +3 -> 2 for ghost 1 for vertex and not cell
        if load_ghost:  # nppb -> points per block side
            ppbs = ppbs_wghost
        else:
            ppbs = bsize + 1

        for name in ["points", "attr", "x", "y", "z"]:
            if name in load_names:
                data_out[name] = initialize_data_array(nblock_out, ppbs,
                                                       int if name == "attr" else float,
                                                       True if name == "points" else False,
                                                       is2D
                                                       )

        # Reading Attributs
        for n in range(1, nblock + 1):
            if (n in blocks) and ("attr" in load_names):
                data_out["attr"][blocks.index(n)] = file.read_block("i4", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("i4")

        # x value
        for n in range(1, nblock + 1):
            if (n in blocks) and ("points" in load_names):
                data_out["points"][blocks.index(n), 0] = file.read_block("f8", load_ghost, ppbs_wghost, is2D)
            elif (n in blocks) and ("x" in load_names):
                data_out["x"][blocks.index(n)] = file.read_block("f8", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("f8")

        # y value
        for n in range(1, nblock + 1):
            if (n in blocks) and ("points" in load_names):
                data_out["points"][blocks.index(n), 1] = file.read_block("f8", load_ghost, ppbs_wghost, is2D)
            elif (n in blocks) and ("y" in load_names):
                data_out["y"][blocks.index(n)] = file.read_block("f8", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("f8")

        # z value
        for n in range(1, nblock + 1):
            if (n in blocks) and ("points" in load_names):
                data_out["points"][blocks.index(n), 2] = file.read_block("f8", load_ghost, ppbs_wghost, is2D)
            elif (n in blocks) and ("z" in load_names):
                data_out["z"][blocks.index(n)] = file.read_block("f8", load_ghost, ppbs_wghost, is2D)
            else:
                file.skip_record("f8")

    if as_matrix:
        for key, data in data_out.items():
            mat_size = (ppbs, ppbs) if is2D else (ppbs, ppbs, ppbs)

            if len(data.shape) == 3:
                data_out[key] = data.reshape((nblock_out, 3) + mat_size)
            else:
                data_out[key] = data.reshape((nblock_out,) + mat_size)
    return data_out, bsize, nblock


def XXD_set_default_args(filename, load_names, read_default, is2D):
    if load_names is None:
        load_names = set()

    if read_default:
        load_names.update({"points"})

    if is2D is None:
        if ".X2D" in filename:
            is2D = True
        else:
            is2D = False
    return load_names, is2D


def adinput2dict(filename, ADs=None, close_disc=True, nstep=-1, as_matrix=False):
    path = os.path.dirname(filename)
    if not path:
        path = "."
    # reading adinput as dict
    out = dict()
    with open(filename, "r") as file:
        # Skipping the header
        file.readline()

        for i_AD, line in enumerate(file, 1):
            # Splitting line
            line = line.strip().split()
            if not line:  # Stopping if line is empty
                break
            if (not ADs is None) and (not i_AD in ADs):
                continue

            # Creating AD container
            AD_name = "AD%d"%i_AD
            out[AD_name] = dict()

            # AD name and discgrid name
            out[AD_name]["name"] = line[0]
            out[AD_name]["discgrid_name"] = line[1]

            # Reading rotor radius
            out[AD_name]["R"] = float(line[2]) / 2

            # Reading turbine location
            out[AD_name]["loc"] = np.array(line[3:6], dtype=float)

            # Reading turbine oriantation (ensuring unit vector)
            o1 = np.array(line[6:9], dtype=float)
            out[AD_name]["o1"] = o1/np.linalg.norm(o1)
            o2 = np.array(line[9:12], dtype=float)
            out[AD_name]["o2"] = o2/np.linalg.norm(o2)
            o3 = np.cross(o1, o2)
            out[AD_name]["o3"] = o3/np.linalg.norm(o3)

            # Reading the disc-grid or making a dummy
            out[AD_name]["discgrid"] = dict()
            out[AD_name]["discgrid"]["points"], nr, ntheta = read_discgrid(out[AD_name]["discgrid_name"],
                                                                       path, close_disc, as_matrix)
            out[AD_name]["nr"] = nr
            out[AD_name]["ntheta"] = ntheta

            # Reading disc data (if present)
            files_in_path = np.array(os.listdir(path))
            for field_name in ["FN", "FT", "VN", "VT"]:
                loc = np.array([(".AS%s%03d"%(field_name, i_AD) in name) for name in files_in_path])
                if any(loc):
                    out[AD_name]["discgrid"][field_name] = read_discgrid_field(os.path.join(path, files_in_path[loc][0])
                                                                               , nr, ntheta, close_disc, nstep, as_matrix)

    return out


def initialize_data_array(nblocks, bsize, dtype=float, vec=False, is2D=False):
    exp = 2 if is2D else 3
    if vec:
        return np.zeros([nblocks, 3, bsize ** exp], dtype=dtype)
    else:
        return np.zeros([nblocks, bsize ** exp], dtype=dtype)


def is_data_field(dtype, action):
    return ((dtype == "REAL") or (dtype == "INTERGER")) and action == "SCATTER"


def read_data(file, dtype, action, size, nblock, data=None, blocks=None, load_ghost=False, bsize=0, is2D=False,
              skip=False):
    bsize_wghost = bsize if load_ghost else bsize + 2
    # Integer of size 1
    if ((dtype == "INTEGER") or (dtype == "LOGICAL")) and (size == 1) and (
            action == "BCAST"):
        if skip:
            file.skip_record("I")
        else:
            data = file.read_record("I")[0]

    # Integers with size large than 1 and BCST (common to all processes)
    elif ((dtype == "INTEGER") or (dtype == "LOGICAL")) and (action == "BCAST"):
        if skip:
            file.skip_record("i4")
        else:
            data = file.read_record("i4")

    # Integers with size larger than 1 and SCATTER (from each block)
    elif ((dtype == "INTEGER") or (dtype == "LOGICAL")) and (action == "SCATTER"):
        if skip:
            for iblock in range(1, nblock + 1):
                file.skip_record("i4")
        else:
            if data is None:
                data = initialize_data_array(len(blocks), bsize, int, False, is2D)
            for iblock in range(1, nblock + 1):
                if iblock in blocks:
                    data[blocks.index(iblock), :] = file.read_block("i4", load_ghost, bsize_wghost, is2D)
                else:
                    file.skip_record("i4")

    # Float of size 1
    elif (dtype == "REAL") and (size == 1) and (action == "BCAST"):
        if skip:
            file.skip_record("f")
        else:
            data = file.read_record("f")[0]

    # Float with size large than 1 and BCST (common to all processes)
    elif (dtype == "REAL") and (action == "BCAST"):
        if skip:
            file.skip_record("f8")
        else:
            data = file.read_record("f8")

    # Float with size larger than 1 and SCATTER (from each block)
    elif (dtype == "REAL") and (action == "SCATTER"):
        if skip:
            for iblock in range(1, nblock + 1):
                file.skip_record("f8")
        else:
            if data is None:
                data = initialize_data_array(len(blocks), bsize, float, False, is2D)
            for iblock in range(1, nblock + 1):
                if iblock in blocks:
                    data[blocks.index(iblock), :] = file.read_block("f8", load_ghost, bsize_wghost, is2D)
                else:
                    file.skip_record("f8")

    # Char with set size
    elif (dtype == "CHARACTER") and (action == "BCAST"):
        if skip:
            file.skip_record("S%i" % size)
        else:
            data = file.read_record("S%i" % size)[0].decode().strip()
    return data


def read_data_header(file):
    name = file.read_record("S20")[0].decode().strip()
    dtype = file.read_record("S20")[0].decode().strip()
    action = file.read_record("S20")[0].decode().strip()
    size = file.read_record("I")[0]
    return name, dtype, action, size


def read_discgrid(filename, path, close_disc, as_matrix):
    if os.path.isfile(os.path.join(path, filename)):
        # Reading the grid file
        with open(os.path.join(path, filename), "r") as gridfile:
            nr, ntheta, ndim = np.array(gridfile.readline().strip().split(), dtype=int)
            if close_disc:
                ntheta += 1
                points = np.zeros((nr, ntheta, ndim))
                points[:, :-1, :] = np.loadtxt(gridfile).reshape([nr, ntheta-1, ndim])
                points[:, -1, :] = points[:, 0, :]
            else:
                points = np.loadtxt(gridfile).reshape([nr, ntheta, ndim])
    else:
        # Making a dummy grid instead of reading
        nr, ntheta, ndim = [2, 100, 3]
        if close_disc:
            theta = np.linspace(-np.pi, np.pi, ntheta)
        else:
            theta = np.linspace(-np.pi, np.pi, ntheta + 1)[:-1]
        points = np.zeros((nr, ntheta, ndim))
        points[1, :, 0] = np.cos(theta)
        points[1, :, 1] = np.sin(theta)
    if as_matrix:
        return points.T, nr, ntheta
    else:
        return points.T.reshape((ndim, nr*ntheta)), nr, ntheta


def read_discgrid_field(filename, nr, ntheta, close_disc, nstep, as_matrix):
    # Cell center data
    data = np.loadtxt(filename)
    if len(data.shape) == 1:
        ns = data[0]
        data = data[1:]
    else:
        ns = data[nstep, 0]
        data = data[nstep, 1:]
    if close_disc:
        points = data.reshape([ntheta-1, nr - 1])
    else:
        points = data.reshape([ntheta, nr-1])[:-1, :]

    if as_matrix:
        return points
    else:
        return points.flatten()


def read_restart_version(file):
    restart_version = file.read_record("S20")[0].decode().strip()
    restart_version_type = file.read_record("S20")[0].decode().strip()
    return restart_version, restart_version_type


def sim2dict(filename_RST, filename_XXD=None, load_names=None, read_default=True, blocks=None, load_ghost=False,
             as_matrix=False, is2D=None, vectors=None):
    # If no grid filename is set
    filename_XXD, load_names, vectors, fields_in_vectors, is2D = sim_set_default_args(filename_RST, filename_XXD,
                                                                                      load_names, read_default, vectors,
                                                                                      is2D)
    # Read grid data
    XXD, bsize_XXD, nblock_XXD = XXD2dict(filename_XXD, load_names, read_default, blocks, load_ghost, as_matrix, is2D)

    # Read restart data
    RST, bsize_RST, nblock_RST = RST2dict(filename_RST, load_names, read_default, blocks, load_ghost, as_matrix, is2D,
                                          vectors)

    # Testing bsize and nblock
    if bsize_RST != bsize_XXD:
        raise ValueError(f"bsize for the XXD-file ({bsize_XXD}) and RST-file ({bsize_RST}) do not match")
    if nblock_XXD != nblock_RST:
        raise ValueError(f"nblock for the XXD-file ({nblock_XXD}) and RST-file ({nblock_RST}) do not match")
    RST.update(XXD)
    return RST, bsize_XXD, nblock_XXD


def sim2names(filename_RST, name_out="fields"):
    """ Reads names in .RST file and addes attr
        name_out='fields' -> only data fields (same shape as grid data)
        name_out='fields info' -> only data fields as well as information about the data (dtype, action, size)
        name_out='all' -> outputs all data names
        name_out='all info' -> returns all data names and information about the data (dtype, action, size)
    """
    data_out = RST2names(filename_RST, name_out)
    if isinstance(data_out, set):
        data_out.add("attr")
    else:
        data_out["attr"] = dict(dtype="INTEGER", action="SCATTER", size=data_out["u"]["size"])
    return data_out


def sim_set_default_args(filename_RST, filename_XXD, load_names, read_default, vectors, is2D):
    # If no grid filename is set
    if filename_XXD is None:
        path = os.path.dirname(filename_RST)
        if path == "":
            path = "."
        proj_name = os.path.basename(filename_RST)
        proj_name = proj_name[:proj_name.index(".RST")]
        if proj_name + ".X3D" in os.listdir(path):
            filename_XXD = os.path.join(path, proj_name + ".X3D")
        elif proj_name + ".X2D" in os.listdir(path):
            filename_XXD = os.path.join(path, proj_name + ".X2D")
        else:
            raise ValueError(
                "Could not find grid a suitable .XXD file in current dir with project name: %s" % proj_name)

    load_names, is2D = XXD_set_default_args(filename_XXD, load_names, read_default, is2D)
    load_names, vectors, fields_in_vectors = RST_set_default_args(load_names, read_default, vectors)
    return filename_XXD, load_names, vectors, fields_in_vectors, is2D


def XXD2vtk(filename, load_attr=True, blocks=None, load_ghost=False, is2D=None, mblock=None):
    # Initializing multiblock
    if mblock is None:
        mblock = vtkMultiBlockDataSet()

    load_names, is2D = XXD_set_default_args(filename, {"attr"} if load_attr else None, True, is2D)

    XXD, bsize, nblock_out = XXD2dict(filename, load_names, True, blocks, load_ghost, False, is2D)

    # Computing domain size and number of blocks
    ppbs = bsize + 3 if load_ghost else bsize + 1
    if blocks is None:
        blocks = range(1, nblock_out + 1)
    nblock = len(blocks)

    # Setting the number of blocks
    mblock.SetNumberOfBlocks(nblock)

    # Setting block data
    for ib, i_block in enumerate(blocks):
        # Converting numpy array to vtk points
        points = vtkPoints()
        points.SetData(dsa.numpyTovtkDataArray(XXD["points"][ib].T, "Points"))

        # Creating the Structured Grid for the block
        sgrid = vtkStructuredGrid()
        if is2D:
            sgrid.SetExtent(0, ppbs - 1, 0, ppbs - 1, 0, 0)
        else:
            sgrid.SetExtent(0, ppbs - 1, 0, ppbs - 1, 0, ppbs - 1)
        sgrid.SetPoints(points)

        # Loading data
        if "attr" in XXD:
            sgrid.GetPointData().AddArray(dsa.numpyTovtkDataArray(XXD["attr"][ib].T, "attr"))

        # Adding the sgrid to mblock
        mblock.SetBlock(ib, sgrid)

        # Setting block name
        mblock.GetMetaData(ib).Set(vtkCompositeDataSet.NAME(), "Block %s" % i_block)

    return mblock


def sim2vtk(filename_RST, filename_XXD=None, load_names=None, read_default=True, blocks=None, load_ghost=False,
            is2D=None, vectors=None, mblock=None):

    # Initializing multiblock
    if mblock is None:
        mblock = vtkMultiBlockDataSet()

    # Reading data
    filename_XXD, load_names, vectors, fields_in_vectors, is2D = sim_set_default_args(filename_RST, filename_XXD,
                                                                                      load_names, read_default, vectors,
                                                                                      is2D)
    if "points" not in load_names:  # Forcing points to always be present
        load_names.add("points")
    sim, bsize, nblock_out = sim2dict(filename_RST, filename_XXD, load_names, read_default, blocks, load_ghost,
                                      False, is2D, vectors)

    # Computing domain size and number of blocks
    ppbs = bsize+3 if load_ghost else bsize+1
    if blocks is None:
        blocks = range(1, nblock_out+1)
    nblock = len(blocks)

    # Setting the number of blocks
    mblock.SetNumberOfBlocks(nblock)


    # Setting block data
    for ib, i_block in enumerate(blocks):
        # Converting numpy array to vtk points
        points = vtkPoints()
        points.SetData(dsa.numpyTovtkDataArray(sim["points"][ib].T, "Points"))

        # Creating the Structured Grid for the block
        sgrid = vtkStructuredGrid()
        if is2D:
            sgrid.SetExtent(0, ppbs-1, 0, ppbs-1, 0, 0)
        else:
            sgrid.SetExtent(0, ppbs-1, 0, ppbs-1, 0, ppbs-1)
        sgrid.SetPoints(points)

        # Loading data
        for name in sim.keys():
            if "points" == name:
                continue
            sgrid.GetPointData().AddArray(dsa.numpyTovtkDataArray(sim[name][ib].T, name))

        # Adding the sgrid to mblock
        mblock.SetBlock(ib, sgrid)

        # Setting block name
        mblock.GetMetaData(ib).Set(vtkCompositeDataSet.NAME(), "Block %s"%i_block)

    return mblock


def adinput2vtk(filename, ADs=None, close_disc=True, mblock=None):
    if mblock is None:
        mblock = vtkMultiBlockDataSet()

    # Reading data
    data = adinput2dict(filename, ADs, close_disc)

    mblock.SetNumberOfBlocks(len(data))

    for i_AD, (AD_name, AD_data) in enumerate(data.items()):

        # Making vtk points
        points = vtkPoints()
        points.SetData(dsa.numpyTovtkDataArray(AD_data["discgrid"]["points"].T, "Points"))

        # Making vtkStructuredGrid
        disc = vtkStructuredGrid()
        disc.SetExtent(0, AD_data["nr"] - 1, 0, AD_data["ntheta"] - 1, 0, 0)
        disc.SetPoints(points)

        # Setting data
        cell_data = disc.GetCellData()
        for name, data in AD_data["discgrid"].items():
            if name == "points":
                continue
            else:
                vtk_arr = dsa.numpy_support.numpy_to_vtk(data)
                vtk_arr.SetName(name)
                cell_data.AddArray(vtk_arr)

        # Transformation
        trans = vtkTransform()
        # Rotation matrix
        transfo_mat = vtkMatrix4x4()
        rot_mat = np.array([AD_data["o2"], AD_data["o3"], AD_data["o1"]]).T
        for i in range(0, 3):
            for j in range(0, 3):
                transfo_mat.SetElement(i, j, rot_mat[i, j])

        transfo_mat.SetElement(0, 3, AD_data["loc"][0])
        transfo_mat.SetElement(1, 3, AD_data["loc"][1])
        transfo_mat.SetElement(2, 3, AD_data["loc"][2])

        trans.SetMatrix(transfo_mat)
        # Translating
        #trans.Translate(*AD_data["loc"])
        # Scaling up
        trans.Scale(*(3 * [AD_data["R"]]))

        # Making transformation filter for the disc
        trans_poly = vtkTransformFilter()
        trans_poly.SetInputDataObject(0, disc)
        trans_poly.SetTransform(trans)
        trans_poly.Update()

        # Adding to multiblock output
        mblock.SetBlock(i_AD, trans_poly.GetOutputDataObject(0))
        mblock.GetMetaData(i_AD).Set(vtkCompositeDataSet.NAME(), AD_name)
    return mblock


