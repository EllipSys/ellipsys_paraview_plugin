import inspect
import os
import ellipsys2vtk
import ellipsys2vtk.util
import ellipsys2vtk.ellipsys2dict

filename_out = os.path.join("..", "EllipSysParaViewPlugin.py")

# Insert module
with open(filename_out, "w") as file:
    file.write(
        """from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smdomain,
    smhint,
    smproperty,
    smproxy,
)
import numpy as np
from vtkmodules.vtkCommonCore import vtkDataArraySelection
import os
from vtk.numpy_interface import dataset_adapter as dsa
from vtk import (vtkMultiBlockDataSet, vtkPoints, vtkStructuredGrid, vtkMatrix4x4,
                 vtkTransformFilter, vtkTransform, vtkArrayCalculator, vtkCompositeDataSet)
"""
    )

# Inserting readers
with open(filename_out, "a") as file:
    with open("XXDreader.py", "r") as file1:
        file.write("# -------- XXD reader --------- #\n")
        for i in range(1000):
            if "###" in file1.readline():
                break
        file.write(file1.read())
        file.write("\n\n")

    with open("RSTreader.py", "r") as file1:
        file.write("# -------- RST reader --------- #\n")
        for i in range(1000):
            if "###" in file1.readline():
                break
        file.write(file1.read())
        file.write("\n\n")

    with open("adinputreader.py", "r") as file1:
        file.write("# -------- adinput reader --------- #\n")
        for i in range(1000):
            if "###" in file1.readline():
                break
        file.write(file1.read())
        file.write("\n\n")

# Insering ellipsys2vtk methods
with open(filename_out, "a") as file:
    # 2dict
    for name, obj in inspect.getmembers(ellipsys2vtk.ellipsys2dict):
        if "__" in name:
            continue
        elif name in ["np", "os"]:
            continue
        else:
            file.write("".join((inspect.getsourcelines(getattr(ellipsys2vtk.ellipsys2dict, name))[0])))
            file.write("\n\n")

    # 2vtk
    file.write("".join((inspect.getsourcelines(ellipsys2vtk.ellipsys2vtk.XXD2vtk))[0]))
    file.write("\n\n")
    file.write("".join((inspect.getsourcelines(ellipsys2vtk.ellipsys2vtk.sim2vtk))[0]))
    file.write("\n\n")
    file.write("".join((inspect.getsourcelines(ellipsys2vtk.ellipsys2vtk.adinput2vtk))[0]))
    file.write("\n\n")





