from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smdomain,
    smhint,
    smproperty,
    smproxy,
)
from vtk.numpy_interface import dataset_adapter as dsa
from vtk import (vtkMultiBlockDataSet, vtkPoints, vtkStructuredGrid,
                 vtkTransformFilter, vtkTransform, vtkArrayCalculator, vtkCompositeDataSet)
from ellipsys2vtk.ellipsys2vtk import adinput2vtk

###

@smproxy.reader(
    name="EllipSys adinput.dat file reader",
    filename_patterns="adinput.dat",
    file_description="EllipSys adinput.dat file",
    support_reload=False,
)
class EllipSysADinputReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet"
        )
        self._filename = None
        self._ADs = None
        self._close_disc = True
        self._projname = "grid"

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self.Modified()

    @smproperty.stringvector(name="ProjectName", default_values="grid")
    def ProjectName(self, projname):
        if self._projname != projname:
            self._projname = projname
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
        name="SetCloseDisc"
        label="Close Disc Mesh"
        command="SetCloseDisc"
        default_values="1"
        number_of_elements="1">
        <BooleanDomain name="bool" />
    </IntVectorProperty>""")
    def SetCloseDisc(self, close_disc):
        if self._close_disc != close_disc:
            self._close_disc = close_disc
            self.Modified()

    @smproperty.stringvector(name="ADs", default_values="All")
    def SetADs(self, ADs_str):
        if ADs_str is None:
            self._ADs = None
            self.Modified()
        elif "None" in ADs_str:
            self._ADs = None
            self.Modified()
        elif "All" in ADs_str:
            self._ADs = None
            self.Modified()
        else:
            blocks = []
            for val in ADs_str.split(","):
                if ":" in val:
                    vals = [int(va) for va in val.split(":")]
                    if not vals:
                        break
                    blocks += list(range(*vals))
                else:
                    blocks += [int(val)]
            self._ADs = blocks
            self.Modified()

    def RequestData(self, request, inInfoVec, outInfoVec):
        # Initalizing the vtkMultiBlockDataSet
        output = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(outInfoVec))

        output = adinput2vtk(filename=self._filename, ADs=self._ADs, close_disc=self._close_disc, mblock=output)
        return 1
