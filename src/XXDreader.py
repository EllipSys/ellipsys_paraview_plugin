from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smdomain,
    smhint,
    smproperty,
    smproxy,
)
from vtk.numpy_interface import dataset_adapter as dsa
from vtk import (vtkMultiBlockDataSet, vtkPoints, vtkStructuredGrid,
                 vtkTransformFilter, vtkTransform, vtkArrayCalculator, vtkCompositeDataSet)
from ellipsys2vtk.ellipsys2vtk import XXD2vtk

###

@smproxy.reader(
    name="EllipSys XXD file reader",
    extensions=["X3D", "X2D"],
    file_description="EllipSys XXD file",
    support_reload=True,
)
class EllipSysXXDReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet"
        )
        self._filename = None
        self._load_attr = True
        self._blocks = None
        self._load_ghost = False
        self._is2D = None

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=["X3D", "X2D"],
        file_description="EllipSys XXD file"
    )
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
            name="Load attr"
            label="Load attr (boundary conditions)"
            command="SetLoadAttr"
            default_values="1"
            number_of_elements="1">
            <BooleanDomain name="bool" />
        </IntVectorProperty>""")
    def SetLoadAttr(self, load_attr):
        if self._load_attr != load_attr:
            self._load_attr = load_attr
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
                name="Load ghost cells"
                label="Load ghost cells"
                command="SetLoadGhost"
                default_values="0"
                number_of_elements="1">
                <BooleanDomain name="bool" />
            </IntVectorProperty>""")
    def SetLoadGhost(self, load_ghost):
        if self._load_ghost != load_ghost:
            self._load_ghost = load_ghost
            self.Modified()

    @smproperty.stringvector(name="Blocks", default_values="All")
    def SetBlocks(self, blocks_str):
        if blocks_str is None:
            self._blocks = None
            self.Modified()
        elif "None" in blocks_str:
            self._blocks = None
            self.Modified()
        elif "All" in blocks_str:
            self._blocks = None
            self.Modified()
        else:
            blocks = []
            for val in blocks_str.split(","):
                if ":" in val:
                    vals = [int(va) for va in val.split(":")]
                    if not vals:
                        break
                    blocks += list(range(*vals))
                else:
                    blocks += [int(val)]
            self._blocks = blocks
            self.Modified()

    def RequestData(self, request, inInfoVec, outInfoVec):
        # Initalizing the vtkMultiBlockDataSet
        output = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(outInfoVec))


        # Reading X3D file and setting data as m-blocks
        XXD2vtk(self._filename, self._load_attr, self._blocks, self._load_ghost, self._is2D, output)
        return 1