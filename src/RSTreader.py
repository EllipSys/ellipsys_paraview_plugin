from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smdomain,
    smhint,
    smproperty,
    smproxy,
)
from vtk.numpy_interface import dataset_adapter as dsa
from vtk import (vtkMultiBlockDataSet, vtkPoints, vtkStructuredGrid,
                 vtkTransformFilter, vtkTransform, vtkArrayCalculator)
from vtkmodules.vtkCommonCore import vtkDataArraySelection
import os
from ellipsys2vtk.ellipsys2vtk import sim2vtk
from ellipsys2vtk.ellipsys2dict import sim2names

###

@smproxy.reader(
    name="EllipSys RST file reader",
    extensions=["RST.01"],
    file_description="EllipSys RST file",
    support_reload=False,
)
class EllipSysRSTReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(
            self, nInputPorts=0, nOutputPorts=1, outputType="vtkMultiBlockDataSet"
        )
        # Initilize filename vars
        self._filename = None
        self._project_name = None
        self._RSTpath = None
        self._filename_grid = None
        self._blocks = None
        self._load_ghost = False
        self._is2D = None

        # Selection arrays
        self._field_names = vtkDataArraySelection()
        # Adding observers (adding Modification event)
        self._field_names.AddObserver("ModifiedEvent", createModifiedCallback(self))

    @smproperty.stringvector(name="FileName", label="RST File")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=["RST.01"],
        file_description="EllipSys RST file"
    )
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self._RSTpath = os.path.dirname(filename)
            self._project_name = os.path.basename(filename).replace(".RST.01", "")
            if self._filename_grid is None:
                self.SetXXDFilename_from_projname()
            self.Modified()

    def SetXXDFilename_from_projname(self):
        if os.path.isfile(os.path.join(self._RSTpath, self._project_name + ".X3D")):
            self._filename_grid = os.path.join(self._RSTpath, self._project_name + ".X3D")
        elif os.path.isfile(os.path.join(self._RSTpath, self._project_name + ".X2D")):
            self._filename_grid = os.path.join(self._RSTpath, self._project_name + ".X2D")
        self.Modified()

    @smproperty.stringvector(name="XXDfilename", default_values="From project name", label="XXD File")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=["X3D", "X2D"],
        file_description="EllipSys X3D or X2D file"
    )
    def SetXXDFileName(self, filename):
        if not filename:
            self.SetXXDFilename_from_projname()
        elif not (("X3D" in filename) or ("X2D" in filename)):
            self.SetXXDFilename_from_projname()
        else:
            self._filename_grid = filename
            self.Modified()

    @smproperty.xml("""<IntVectorProperty
                    name="Load ghost cells"
                    label="Load ghost cells"
                    command="SetLoadGhost"
                    default_values="0"
                    number_of_elements="1">
                    <BooleanDomain name="bool" />
                </IntVectorProperty>""")
    def SetLoadGhost(self, load_ghost):
        if self._load_ghost != load_ghost:
            self._load_ghost = load_ghost
            self.Modified()

    @smproperty.stringvector(name="Blocks", default_values="All")
    def SetBlocks(self, blocks_str):
        if blocks_str is None:
            self._blocks = None
            self.Modified()
        elif "None" in blocks_str:
            self._blocks = None
            self.Modified()
        elif "All" in blocks_str:
            self._blocks = None
            self.Modified()
        else:
            blocks = []
            for val in blocks_str.split(","):
                if ":" in val:
                    vals = [int(va) for va in val.split(":")]
                    if not vals:
                        break
                    blocks += list(range(*vals))
                else:
                    blocks += [int(val)]
            self._blocks = blocks
            self.Modified()

    @smproperty.dataarrayselection(name="Field Data")
    def GetScalarDataSelection(self):
        return self._field_names

    def RequestInformation(self, request, inInfoVec, outInfoVec):
        executive = self.GetExecutive()
        outInfo = outInfoVec.GetInformationObject(0)

        field_names = sim2names(self._filename)

        # Setting scalar cell names to selection array
        for name in field_names:
            self._field_names.AddArray(name, name == "p")
        self._field_names.AddArray("Vel", True)

        return 1

    def RequestData(self, request, inInfoVec, outInfoVec):
        # Initilizing multiblock
        output = dsa.WrapDataObject(vtkMultiBlockDataSet.GetData(outInfoVec))

        load_names = set()
        for i in range(self._field_names.GetNumberOfArrays()):
            sname = self._field_names.GetArrayName(i)
            if self._field_names.ArrayIsEnabled(sname):
                load_names.add(sname)

        mblock = sim2vtk(self._filename, self._filename_grid, load_names, False, self._blocks,
                         self._load_ghost, self._is2D, None, output)

        return 1


# From: https://github.com/Kitware/ParaView/blob/master/Examples/Plugins/PythonAlgorithm/PythonAlgorithmExamples.py
def createModifiedCallback(anobject):
    import weakref
    weakref_obj = weakref.ref(anobject)
    anobject = None

    def _markmodified(*args, **kwars):
        o = weakref_obj()
        if o is not None:
            o.Modified()

    return _markmodified

