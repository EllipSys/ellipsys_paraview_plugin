from paraview.simple import *

Nframes = 60
pathname = 'nrel5mw_objs/'
obj_name = 'nrel5mw_simple_'

## Convert from .obj to .vtp ####################
for k in range(Nframes):
    filename = pathname + obj_name + str(k+1).zfill(6) + '.obj' 
    print(filename)
    reader = OpenDataFile(filename)
    vtpname = 'turbine_vtp/turbine_' + str(k+1).zfill(6) + '.vtp' 
    writer = CreateWriter(vtpname)
    writer.UpdatePipeline()



## Convert from .vtp to .pvd ####################
outfile = 'nrel5mw.pvd'
f = open(outfile,'w')

f.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n')
f.write('  <Collection>\n')

# Right now, the timestep is just set equal to frame number, but 
# could be different.
for k in range(Nframes):
    turb = 'turbine_vtp/' + 'turbine_' + str(k+1).zfill(6) + '.vtp' 
    tmp = '    <DataSet timestep="%.1f" file="%s"/>'%(float(k+1),turb)
    f.write(tmp + '\n')
    print(tmp)


f.write('  </Collection>\n')
f.write('</VTKFile>')
f.close()
