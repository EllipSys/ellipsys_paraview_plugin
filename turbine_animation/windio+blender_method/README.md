# Turbine animation in Paraview using windio2cad and Blender

The procedure described here is inspired by the animations from a recent paper<sup>[1](#Liu2021)</sup>. In this small tutorial, we will create a NREL5MW turbine and animate a single rotation.

![Alt Text](output.gif)

## Pre-requisites

You need to download the following software:

- windio2cad (git clone https://github.com/IEAWindTask37/windio2cad.git)
- openscad (sudo snap install openscad)
- blender (sudo snap install blender --channel=2.93lts/stable --classic)

There are three basic steps:

1. Creating a turbine animation in Blender (the final Blender file is included in this repo, so this step can be skipped).
2. Exporting the animation to a Paraview-friendly format.
3. Loading the animation into Paraview.


## (Optional) Creating a turbine with windio2cad and Blender

Install windio2cad (see github instructions on https://github.com/IEAWindTask37/windio2cad) and go to the windio2cad folder:

    $ cd git/windio2cad
    $ python -m windio2cad --input nrel5mw-semi_oc3.yaml --blade --output blade.stl --openscad /snap/bin/openscad

If successful, this should create blade.stl, which can be opened in Blender (File > Import > Stl) and duplicated twice. A nacelle can be created from a "box", while the tower and spinner can both be created from a "cylinder" (see for example https://www.youtube.com/watch?v=SMVu3o6cp7I); in Blender, these objects can be created from the menu on the left.

![](blender_create_boxcylinder.png)

Put the "pieces" together to obtain the final result:

![](blender_turbine.png)

Finally, select the three blades and the spinner (click and hold shift), and press ctrl+j to join them together into a single object. To animate rotor rotation, press the small dot next the rotation axis (right side on the above picture). Then open the graph editor (left side of the above picture) and draw the rotation curve.

The final result is in the *nrel5mw_simple.blend*-file.

## Export animation from Blender and format conversion

Go to File > Export > .Obj, and export the animation (remember to check "Animation", "Forward", "Up" and "Triangulate faces"):

![](blender_export.png)

If successful, it will create a folder with a lot of .obj and .mtl files, i.e. one for each frame. Now these need to be converted to the Paraview-friendly format, .pvd, which is done with a simple python script (need to run it with pvpython, which you should have access to, if you have a Paraview installation):

    $ mkdir turbine_vtp
    $ pvpython obj2pvd.py

If succesful, it creates a *nrel5mw.pvd*-file. Actually, both *.obj and *.vtp-files can be opened with Paraview, but the advantage of a .pvd-file is that a timestep can be associated with each frame.

## Load animation in Paraview

Open the nrel5mw.pvd-file in Paraview and press "apply". If loaded properly, one should be able to press the "play" buttom to start the animation.

To create a gif, File > Save animation, and output as png-files. Afterwards:

    $ cd my_nrel5mw_pngs
    $ ffmpeg -i nrel5mw.%04d.png -vf scale=512:-1 output.gif

If successful, it will create a gif like the one shown at the beginning of this page.


## References
<a name="Liu2021">1</a>:
L. Liu and  R. Stevens,
*Enhanced wind-farm performance using windbreaks*, Physical Review Fluids,
2021, https://doi.org/10.1103/PhysRevFluids.6.074611 (supplementary animation by S. Gadde)
