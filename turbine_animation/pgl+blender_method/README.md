# Creating a turbine animation with PGL and Blender

This is similar to the windio2cad+Blender procedure, but now the turbine is created with PGL instead.


## Pre-requisites

You need to download the following software:

- PGL (git clone https://gitlab.windenergy.dtu.dk/frza/PGL.git)
- blender (see windio+blender readme)



## Creating a full rotor with PGL

Install PGL (see instructions on https://gitlab.windenergy.dtu.dk/frza/PGL) and go to examples folder:

    $ cd git/PGL/PGL/examples
    $ conda activate pgl (my anaconda pgl environment is simply called "pgl")
    $ python blademesher_nacelle_example.py


If successful, it will create a plot3d file of a DTU10MW rotor called *DTU_10MW_RWT_nacelle_mesh.xyz*. This can be opened and visualized directly with Paraview:

    $ paraview DTU_10MW_RWT_nacelle_mesh.xyz

![](DTU10MW_rotor.png)


## Importing to Blender

Continuing in Paraview, use the following steps to convert the .xyz-file to a .ply-file;

1. Apply "Merge blocks"-filter
2. Apply "Extract surface"-filter
3. Highlight ExtractSurface1 in the pipeline browser, then File > Save data, where you should choose the .ply-extension

![](convert2ply.png)

This will create .ply-file, which is almost as small in file size as the original .xyz-file, and which can be opened directly in Blender with File > Import > Stanford (.ply).



## Creating an animation in Blender

In Blender, you can create a nacelle and a tower, and animate the rotation. The procedure is the same as explained in the windio+blender readme, so it is not repeated here.
