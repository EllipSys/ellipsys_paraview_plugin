## Running Paraview remotely on Sophia

It is possible to have Paraview on Sophia, where all the data and rendering is done at Sophia and the client is only
displaying the rendering (this avoids the need to transfer the data from Sophia to your own computer). <br>
We need to set it up at both *Sophia* and your *Desktop*. It should be noted that the Paraview version
on Sophia and the Desktop should be the same (this setup is tested with version 5.10.0)!

#### Pre-requisites
- Pre-requisite 1: Clone ellipsys_paraview_plugin to $HOME/git/ on Sophia. Also, clone ellipsys_paraview_plugin to your local pc, if you have not done this already. <br>
- Pre-requisite 2: Download a headless (i.e. without GUI) Paraview installation to your Sophia home directory and rename it to "ParaView_headless". It can be downloaded from https://www.paraview.org/download/ (choose the osmesa version).<br>
- Pre-requisite 3: Add the following to your Sophia .bashrc:
```bash
# at the end of .bashrc on sophia
export PV_PLUGIN_PATH="$HOME/git/ellipsys_paraview_plugin"
```


#### Start-up of Paraview server
1. Find startpv_sophia in ellipsys_paraview_plugin on your local pc. Change mchba --> your initials in the thrid line. <br>
2. Execute ./startpv_sophia<br>
3. If step 2 is successful, a series of informative messages should be printed and a Paraview server should now be running. You can check with "squeue", if a 30 min job called "pvserver" is running. <br><br>

Note: You might need to "chmod 777 startpv_sophia". Also, it is required that you have already made a ssh-keygen for Sophia (if not, I think you will get prompted for password for Sophia?)

#### Connecting to the Paraview server on your local pc

The first time you connect to your Paraview server, a "server configuration" needs to be created in your Paraview installation on your local machine:

1. File > Connect > Load server, and select the "servers.pvsc"-file. Now a new server option is created called "my_remove_pv".
![](first_remote_server_conf.png)

Double-click on this to connect to your Paraview server. This might take 10-15 seconds, if you use a VPN.

2. If the connection is succesful, a prompt with plugins appears. Click "load new" and locate the ellipsys_paraview_plugin folder on Sophia (pre-requisite 1) and press "ok". "loaded" should now appear to the right of "EllipsysParaviewPlugin" and you can now press "close". In the pipeline browser box to left, your connection name and port number should now be visible. <br>
3. Now you should be able to load .RST-files directly from Sophia! This means that you no longer need to download .RST, .X3D nor adinput.dat files to your local pc. <br><br>

Step 1 and 2 are only necessary the first time you connect to a Sophia Paraview server. The next time you should be able to choose the server created in step 1 and connect to that. Also, the plugin should load automatically, because of pre-requisite 3.

## Running Paraview through mount
This works on both Sophia and gbar. <br>
Add the following to your local .bashrc:
```bash
# at the end of .bashrc
export RU=mchba (change mchba --> your initials)
alias ms='sshfs -o ssh_command="ssh -i ~/.ssh/id_rsa" $RU@sophia.dtu.dk/home/$RU ~/mount/sophia/; cd ~/mount/sophia/'
alias mgbar='sshfs -o ssh_command="ssh -i ~/.ssh/id_rsa" $RU@login.gbar.dtu.dk: ~/mount/gbar/; cd ~/mount/gbar/'
```

Save file and "source .bashrc" in the terminal. Note, I have used ssh-keygen and if you haven't, then delete the ssh-stuff from the above commands.

#### Mount on Sophia
1. mkdir ~/mount/sophia (this step is only necessary, the first time you try to mount) <br>
2. ms (this will mount sophia on your local pc and change dir to the folder) <br>
3. From your local Paraview, you can now access the files on Sophia. The first time you open a file, it might take a while and Paraview may hang for some time, because the file is de facto being downloaded behind the hood. <br>

A similar procedure can be done with gbar.

## Running remote vs. mount
It is very easy to use mount, when you first have created aliases and the mount directories. On the other hand, it takes a little effort to open a remote Paraview server and connect to it, although most of the process is automated through the startpv_sophia bash script. The advantage of the remote method is illustrated by the following example: <br>

A .RST-file of size 126 MB takes 1 min to open with mount. The same file only takes about 1 sec to open with the remote Paraview server. This comparison was made through a half-bad wifi connection at home with VPN. If you are at Risø, it is another talk: Here the file loads almost instantly for both the remote and mount methods. Rule of thumb for loading files through mount: 1 GB / 7 min (at home through VPN) and 1 GB / 20 sec (wired connection at Risø). <br>
For a very large .RST-file, e.g. on the order of 10 GB, the file will not fit into memory of many laptops, so then you don't have other options than to use the remote server method.

A disadvantage of the remote method, is that Paraview might feel a bit slow, when for example rotating or zooming in the render view, especially for large files or bad internet connections.
