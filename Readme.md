# Ellipsys Paraview Plugins

Plugins for Paraview (version >= 5.6, [Download Paraview](https://www.paraview.org/download/) - it is recommended to use
the latest version, i.e. version 5.10 as of Feb 2022) to read files directly outputted from Ellipsys and dedicated filters.

## Installation

1. Download or clone the repo
2. Open Paraview and go to the Plugin Manager (*Tools/Manage Plugins*)
3. Load the new plugin (*Load New* and navigate to this repo and select *EllipsysParaviewPlugin.py*)
4. If the plugin should be loaded after Paraview restart, be sure to tick the box *Auto Load* when expanding the plugin
 in the Plugin Manager

## Supported files
- .X3D and .X2D *(Mesh vertices as a multi-block-dataset)*
- adinput.dat *(Reads all ADs with: AD-location, AD-orientation, AD-discgrid - as well as reading ad-out data)*
- RST.01 *(Reads mesh file (X3D or X2D) and data in RST. All data with size matching cell data can be loaded from the data
picker. Only grid level 1 is supported currently)*
- flowdata.nc *(Output of flat terrain pywake_ellipsys simulations. Size is typically only 20% of its .RST-file, hence 5 times faster load-in!)*

Help to add support for more files is much appreciated.

## How to use
The above mentioned file extensions will now be recognized when opening a file.

#### To load RST.01
As an example it is assumed we have a *grid.RST.01* file that we want to read (notice that either the *grid.X3D* file needs to be present
in the same folder or it should be navigated to from the *XXD File*).
1. Open the file manager either from the shortcut in the upper left corner or *File/Open..*
2. Navigate to the *grid.RST.01* file select it and click *OK*
3. On the left the *Data-Picker* is now be visible. Select the data that that you want to load by clicking the check boxes.
4. Click *Apply* to load the data into Paraview. You can select between the different field from the dropdown-menu which
by default is set to *vtkBlockColors*. The representation can be changed from the dropdow-menu which by default is set to *Outline*

It also include the following optional setting:
- *Load only a subset of blocks*. It is possible to only load a subset of the blocks by setting the *Blocks* field. Either single blocks can be set (an integer, e.g. 10 for block 10), or a set of block separated by comma (e.g 10,12,14 for block 10,12,14) or a range of blocks with a semi-colon (e.g. 10:15 for the block between 10 and 14, not including 15). Any combination of these can be done with comma separation (e.g. 10, 12, 14:17 for blok 10,12 and blocks 14 to 16).
- *Load the ghost cells* if the ghost cells needs to be added it is possible to do that by checking the *Load ghost cells* checkbox.

#### To load flowdata.nc (flat terrain)
When loading in flowdata.nc-files, there will be three reader options.

1. Choose "NetCDF Reader". Press the green "Apply" buttom.
2. Click on "flowdata.nc" in the Pipeline Browser, then go to Filters > Alphabetical and choose "load_flowdata", then "Apply". (the load_flowdata filter can be found in the "dedicated_filters"-folder)

Tip: In step 2, instead of going to Filters > Alphabetical, then just use "Ctrl" + "Space" to search for the filter.

## Dedicated filters
Also known as "custom filters" in Paraview. See the readme in the "dedicated_filters"-folder.

## Running Paraview remotely on Sophia

It is possible to run Paraview on Sophia ("server side", all the data and rendering is done at Sophia) and display the results on your local pc ("client side") in real time. This avoids the need to transfer the data from Sophia to your own computer and possible memory constraints of your local pc. A guide can be found in the remote_server folder.

## Running Paraview with mounted Sophia or Gbar

For smaller projects, it might be overkill to run a remote Paraview server. In such cases, one can simply mount his/her home directory of Sophia or Gbar, and run Paraview from the local machine. For more info, see readme in the "remote_server"-folder.

## Uninstalling the plugin
To uninstall the plugin you will have to navigate to the Plugin Manager (*Tools/Manage Plugins*) and select the
*EllipsysParaviewPlugin* and click the *Remove* button.

## Updating the plugin
When updating the plugin to a newer version you need to uninstall the old one, as described above then <b> Restart Paraview</b> (<- important) <br>
Opening Paraview again you can install the plugin as described at the top.

## Sources
Some inspiration for developing this plugin was found at:<br>
[Blog post: *Generic mesh readers under ParaView via the new Python interface*](https://www.litianyi.me/2019/10/27/paraview-python-plugin/)<br>
[Github repo: Plugin from above blog post](https://github.com/tianyikillua/paraview-meshio)<br>
[Paraview Guide (chapter 12)](https://www.paraview.org/paraview-guide/)
