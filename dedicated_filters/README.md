## Dedicated filters

Also known as "custom filters" in Paraview. Go to Tools > Manage Custom Filters (Click "Import" and locate whatever custom filter you want to load in). The custom filter you have imported should now appear in Filters > Alphabetical.

Below are some examples you can try to import:
- load_flowdata: Apply to flowdata.nc - fixes orientation error.
- calc_stresses_flowdata: Calculates Reynolds stresses (uu, vv, ww, uv, uw and uw) using Boussinesq hypothesis.
- calc_stresses: Same as above (to be used when working with .RST.01-files, which have different variable names than flowdata files).

Note, that the calc_stresses filters only work with version 5.10 and only for two-equation turbulence models (keps or komega).
